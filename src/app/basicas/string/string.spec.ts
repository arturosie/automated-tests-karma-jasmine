import { mensaje } from './string';

// Sirve para agrupar pruebas, hace referencia a un conjunto de pruebas
describe('Pruebas de Strings', () => {

    it( 'Debe de regresar un string', () => {
        //Disparamos la funcion y guardamos el return en una constante
        const resp = mensaje('Arturo');

        //Evaluamos la prueba
        expect( typeof resp ).toBe('string');

    });

    it( 'Debe de regresar el nombre enviado', () => {

        const nombre = "Arturo";

        //Disparamos la funcion y guardamos el return en una constante
        const resp = mensaje( nombre);

        //Evaluamos la prueba
        expect( resp ).toContain(nombre);

    });

});

// Prueba en especifico
//it('Debe de regresar un string');