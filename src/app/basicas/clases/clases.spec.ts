import { Jugador } from './clases';

describe('Pruebas de clases', () => {

    const jugador = new Jugador();

    // Ciclos de vida de una prueba
    // Se dispara antes de todas las pruebas
    beforeAll( () => {
        //console.log('beforeAll');
    });

    // Se dispara antes de cada prueba
    beforeEach( () => {
        //console.log('beforeEach');
        jugador.salud = 100;
    });

    // Despues de cada una de las pruebas
    afterEach( () => {
        //console.log('afterEach');
    });
    
    // Despues de todas las pruebas 
    afterAll( () => {
        //console.log('afterAll');
    });

    it('Debe de retornar 80 de salud si recibe 20 de daño', () => {

        const resp = jugador.recibeDanio(20);

        expect( resp ).toBe(80);

    });

    it('Debe de retornar 50 de salud si recibe 50 de daño', () => {

        const resp = jugador.recibeDanio(50);

        expect( resp ).toBe(50);

    });

    it('Debe de retornar 0 de salud si recibe 100 de daño', () => {

        const resp = jugador.recibeDanio(100);

        expect( resp ).toBe(0);

    });

});