export class Jugador {

    salud: number;

    constructor() {
        this.salud = 100;
    }

    recibeDanio( danio: number ) {

        if( danio >= this.salud ) {
            this.salud = 0;
        } else {
            this.salud = this.salud - danio;
        }

        return this.salud;

    }

}