import { obtenerRobots } from './arreglos';

describe('Prueba de arreglos', () => {

    it('Debe de retornar al menos 3 robots', () => {

        const resp = obtenerRobots();
        expect(resp.length).toBeGreaterThanOrEqual(3);

    });

    it('Debe de exisitir MegaMan y Robocop', () => {

        const resp = obtenerRobots();
        expect(resp).toContain('MegaMan');
        expect(resp).toContain('Robocop');

    });

});